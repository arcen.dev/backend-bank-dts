const express = require('express');
const UserController = require('./app/controllers/UserController');

const router = new express.Router();

router.get('/api', UserController.index); //fungsi read

router.post('/api', UserController.create); //fungsi create

router.put('/api/:id', UserController.update); //fungsi edit

router.delete('/api/:id', UserController.destroy); //fungsi delete

module.exports = router;

//